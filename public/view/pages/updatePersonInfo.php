<?php

$person_id = $_GET['id'];
$person = \App\Initialization::getOneNote($person_id);
?>

<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>
<body>
        <h3>Update information</h3>
        <form action="/update/upload" method="post">
            <div class="mb-3 ">
                <input type="hidden" name="id" value="<?= $person['id']?>">
                <label>
                    <input class="form-control" type="text" name="name" value="<?= $person['user_name'];?>">
                </label>
            </div>
            <div class="mb-3">
                <label>
                    <input class="form-control" type="text" name="surname" placeholder="Input surname" value="<?= $person['user_surname'];?>">
                </label>
            </div>
            <div class="mb-3">
            <label>
                    <input class="form-control" type="text" name="patronymic" placeholder="Input patronymic" value="<?= $person['user_patronymic'];?>">
                </label>
            </div>
            <div class="mb-3">
                <label>
                    <input class="form-control" type="date" name="date_birth" placeholder="Input date birth" value="<?= $person['user_date_birth'];?>">
                </label>
            </div>
            <div class="mb-3">
                <label>
                    <input class="form-control" type="email" name="email" placeholder="Input email" value="<?= $person['user_email'];?>">
                </label>
            </div>
            <div class="mb-3">
                <label>
                    <input class="form-control" type="number" name="phone" placeholder="Input phone number" value="<?= $person['user_phone'];?>">
                </label>
            </div>
            <div class="mb-3">
                <label>
                    <input class="form-control" type="text" name="company" placeholder="Input company" value="<?= $person['user_company'];?>">
                </label>
            </div>
            <div class="mb-3">
                <input class="form-control" type="file" name="avatar" placeholder="Download avatar" value="<?= $person['user_photo'];?>">
            </div>
                <button class="btn btn-primary" type="submit">Update</button>
        </form>
    </div>
</body>
</html>
