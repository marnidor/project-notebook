<?php
$person = new \App\Initialization;

?>

<!DOCTYPE html>
<html lang="ru">
<?php require_once 'view/pages/blocks/head.php'?>
<body>
<div class="container">
    <?php require_once 'view/pages/blocks/header.php'?>
</div>
<body>
    <h3 class="text-center">Add new person</h3>
    <form method="post">
        <div class="mb-3">
            <label>
                <input class="form-control" type="text" name="name" placeholder="Input name">
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="text" name="surname" placeholder="Input surname">
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="text" name="patronymic" placeholder="Input patronymic">
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="date" name="date_birth" placeholder="Input date birth">
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="email" name="email" placeholder="Input email"
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="number" name="phone" placeholder="Input phone number">
            </label>
        </div>
        <div class="mb-3">
            <label>
                <input class="form-control" type="text" name="company" placeholder="Input company">
            </label>
        </div>
        <div class="mb-3">
            <input class="form-control" type="file" name="avatar" placeholder="Download avatar">
        </div>
        <button class="btn btn-primary" type="submit">Add new person</button>
    </form>
</body>
</html>
